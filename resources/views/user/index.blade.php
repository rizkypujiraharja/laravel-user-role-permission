@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @include('flash-message')
            <div class="card">
                <div class="card-header">Daftar User</div>

                <div class="card-body">
                    @permission('create-user')
                    <a href="{{ route('user.create') }}">
                        <button class="btn btn-primary">Tambah User</button>
                    </a>
                    <br><br>
                    @endpermission
                <form action="{{ route('user.bulk.delete') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <table class="table">
                        <thead>
                          <tr>
                            @role('admin')
                            <th></th>
                            @endrole
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Action</th>
                          <tr>
                        </thead>
                        <tbody>
                        @forelse ($users as $user)
                          <tr>
                            @role('admin')
                              <th><input type="checkbox" name="ids[]" value="{{ $user->id }}"></th>
                            @endrole
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>
                                  <form action="{{ route('user.destroy', $user) }}" id="{{$user->id}}" method="POST">
                                    @permission('read-user')
                                    <a href="{{ route('user.show', $user) }}">
                                        <button class="btn btn-sm btn-info" type="button">Show</button>
                                    </a>
                                    @endpermission
                                    @permission('update-user')
                                    <a href="{{ route('user.edit', $user) }}">
                                        <button  class="btn btn-sm btn-warning" type="button">Edit</button>
                                    </a>
                                    @endpermission
                                    @permission('delete-user')
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm btn-danger">
                                        Delete
                                    </button>
                                    @endpermission
                                  </form>
                              </td>
                          </tr>
                        @empty
                          <tr>
                              <td colspan="4" align="center">Belum ada data</td>
                          <tr>
                        @endforelse
                        </tbody>
                    </table>
                    @role('admin')
                      <button type="submit" class="btn btn-danger">Bulk Delete</button>
                    @endrole
                </form>
                <br><br>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
