@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Detail User</div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>Nama</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <td>Bergabung</td>
                            <td>{{ $user->created_at->diffForHumans() }}</td>
                        </tr>
                        <tr>
                            <td>Foto</td>
                            <td>
                                @if (!is_null($user->picture))
                                    <img src="{{ $user->getPicture() }}">
                                @else
                                -
                                @endif
                            </td>
                        </tr>
                    <table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
