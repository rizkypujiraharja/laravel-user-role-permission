@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Profile</div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>Nama</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <td>Bergabung</td>
                            <td>{{ $user->created_at->diffForHumans() }}</td>
                        </tr>
                        <tr>
                            <td>Foto</td>
                            <td>
                                @if (!is_null($user->picture))
                                    <img src="{{ $user->getPicture() }}">
                                    <br>
                                @endif
                                <form action="{{ route('updatePicture') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="file" name="picture">
                                    <button type="submit" class="btn btn-primary">Upload & Simpan</button>
                                <form
                            </td>
                        </tr>
                    <table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
