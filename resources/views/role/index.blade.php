@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @include('flash-message')
            <div class="card">
                <div class="card-header">Daftar Role</div>

                <div class="card-body">
                    @permission('create-role')
                    <a href="{{ route('role.create') }}">
                        <button class="btn btn-primary">Tambah Role</button>
                    </a>
                    <br><br>
                    @endpermission
                    <table class="table">
                        <thead>
                          <tr>
                            <th>Nama</th>
                            <th>Tampilan Nama</th>
                            <th>Action</th>
                          <tr>
                        </thead>
                        <tbody>
                        @forelse ($roles as $role)
                          <tr>
                              <td>{{ $role->name }}</td>
                              <td>{{ $role->display_name }}</td>
                              <td>
                                  <form action="{{ route('role.destroy', $role) }}" id="{{$role->id}}" method="POST">
                                    @permission('read-role')
                                    <a href="{{ route('role.show', $role) }}">
                                        <button class="btn btn-sm btn-info" type="button">Show</button>
                                    </a>
                                    @endpermission
                                    @permission('update-role')
                                    <a href="{{ route('role.edit', $role) }}">
                                        <button  class="btn btn-sm btn-warning" type="button">Edit</button>
                                    </a>
                                    @endpermission
                                    @permission('delete-role')
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm btn-danger">
                                        Delete
                                    </button>
                                    @endpermission
                                  </form>
                              </td>
                          </tr>
                        @empty
                          <tr>
                              <td colspan="4" align="center">Belum ada data</td>
                          <tr>
                        @endforelse
                        </tbody>
                    </table>
                    <br><br>
                    {{ $roles->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
