@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Detail Role</div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>Nama</td>
                            <td>{{ $role->display_name }}</td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>{{ $role->description }}</td>
                        </tr>
                        <tr>
                            <td>Hak Akses</td>
                            <td>
                                <ul>
                                @foreach ($role->permissions as $item)
                                    <li>{{ $item->display_name }}
                                @endforeach
                                </ul>
                            </td>
                        </tr>
                    <table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
