<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            [
                'name' => 'create-user',
                'display_name' => 'Tambah User',
            ],
            [
                'name' => 'read-user',
                'display_name' => 'Lihat User',
            ],
            [
                'name' => 'update-user',
                'display_name' => 'Ubah User',
            ],
            [
                'name' => 'delete-user',
                'display_name' => 'Hapus User',
            ],
            [
                'name' => 'create-role',
                'display_name' => 'Tambah Role',
            ],
            [
                'name' => 'read-role',
                'display_name' => 'Lihat Role',
            ],
            [
                'name' => 'update-role',
                'display_name' => 'Ubah Role',
            ],
            [
                'name' => 'delete-role',
                'display_name' => 'Hapus Role',
            ]
        ]);
    }
}
