<?php

use Illuminate\Database\Seeder;
use App\{Role, Permission};

class AttachPermissionToRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Set Admin Permission
        $admin = Role::where('name', 'admin')->first();
        $permissionAdmin = Permission::get();
        $admin->attachPermissions($permissionAdmin);

        //Set Supervisor Permission
        $supervisor = Role::where('name', 'supervisor')->first();
        $permissionSupervisor = Permission::whereIn('name',
                            [
                                'create-user', 'read-user', 'create-role', 'read-role'
                            ])
                            ->pluck('id');
        $supervisor->attachPermissions($permissionSupervisor);


        //Set Guest Permission
        $guest = Role::where('name', 'guest')->first();
        $permissionGuest = Permission::whereIn('name',
                            [
                                'read-user', 'read-role'
                            ])
                            ->pluck('id');
        $guest->attachPermissions($permissionGuest);
    }
}
