<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            [
                'name' => 'admin',
                'display_name' => 'Admin'
            ],
            [
                'name' => 'supervisor',
                'display_name' => 'Supervisor'
            ],
            [
                'name' => 'guest',
                'display_name' => 'Guest'
            ]
        ]);
    }
}
