<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Rizky Puji Raharja',
            'email' => 'rizky.rsj@gmail.com',
            'password' => bcrypt('secret')
        ]);
    }
}
