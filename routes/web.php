<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', function () {
        return view('home');
    });

    Route::get('/home', function () {
        return view('home');
    });


    Route::get('/profile', 'UserController@profile')->name('user.profile');
    Route::post('/profile', 'UserController@updatePicture')->name('updatePicture');

    Route::group(['prefix' => 'user'], function () {
        Route::delete('/delete', 'UserController@bulkDelete')
            ->name('user.bulk.delete')->middleware('role:admin');
        Route::get('/', 'UserController@index')->name('user.index');
        Route::get('/create', 'UserController@create')
            ->name('user.create')->middleware('permission:create-user');
        Route::post('/', 'UserController@store')
            ->name('user.store')->middleware('permission:create-user');
        Route::get('/{user}', 'UserController@show')
            ->name('user.show')->middleware('permission:read-user');
        Route::get('/{user}/edit', 'UserController@edit')
            ->name('user.edit')->middleware('permission:update-user');
        Route::match(['put', 'patch'], '/{user}', 'UserController@update')
            ->name('user.update')->middleware('permission:update-user');
        Route::delete('/{user}', 'UserController@destroy')
            ->name('user.destroy')->middleware('permission:delete-user');
    });

    Route::group(['prefix' => 'role'], function () {
        Route::get('/', 'RoleController@index')->name('role.index');
        Route::get('/create', 'RoleController@create')
            ->name('role.create')->middleware('permission:create-role');
        Route::post('/', 'RoleController@store')
            ->name('role.store')->middleware('permission:create-role');
        Route::get('/{role}', 'RoleController@show')
            ->name('role.show')->middleware('permission:read-role');
        Route::get('/{role}/edit', 'RoleController@edit')
            ->name('role.edit')->middleware('permission:update-role');
        Route::match(['put', 'patch'], '/{role}', 'RoleController@update')
            ->name('role.update')->middleware('permission:update-role');
        Route::delete('/{role}', 'RoleController@destroy')
            ->name('role.destroy')->middleware('permission:delete-role');
    });


});
