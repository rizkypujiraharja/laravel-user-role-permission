<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{User, Role};

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(12);

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();

        return view('user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:30',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8',
            'password_confirmation' => 'required|min:8|same:password',
            'role' => 'required|exists:roles,id'
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->syncRoles($request->role);

        return redirect()->route('user.index')->with('alert-success', 'Berhasil menambah user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::get();
        return view('user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:30',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'nullable|string|min:8',
            'role' => 'required|exists:roles,id'
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        if($request->has('password'))
            $user->password = bcrypt($request->password);
        $user->save();
        $user->syncRoles($request->role);

        return redirect()->route('user.index')->with('alert-success', 'Berhasil mengubah user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('user.index')->with('alert-success', 'Berhasil menghapus user');
    }

    public function profile() {
        $user = \Auth::user();

        return view('user.profile', compact('user'));
    }

    public function updatePicture(Request $request) {
        $this->validate($request,[
            'picture' => 'required|image'
        ]);

        $time = time();

        $originalImage= $request->file('picture');
        $thumbnailImage = \Image::make($originalImage);
        $thumbnailPath = public_path().'/thumbnail/';
        $thumbnailImage->resize(150,150);
        $thumbnailImage->save($thumbnailPath.$time.$originalImage->getClientOriginalName());


        $user = \Auth::user();
        $user->picture=$time.$originalImage->getClientOriginalName();
        $user->save();

        return redirect()->route('user.profile')->with('alert-success', 'Berhasil mengupload');

    }

    public function bulkDelete(Request $request) {
        $users = User::whereIn('id', $request->ids)->delete();

        return redirect()->back()->with('alert-success', 'Berhasil menghapus user');
    }
}
