<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Role, Permission};

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::paginate(12);

        return view('role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::get();

        return view('role.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:30',
            'description'  => 'nullable|string|min:3|max:100'
        ]);

        $role = new Role;
        $role->name = str_slug($request->name);
        $role->display_name = $request->name;
        $role->description = $request->description;
        $role->save();
        $role->syncPermissions($request->permission);

        return redirect()->route('role.index')->with('alert-success', 'Berhasil menambah role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return view('role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $permissions = Permission::get();
        return view('role.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:30',
            'description'  => 'nullable|string|min:3|max:100'
        ]);

        $role->name = str_slug($request->name);
        $role->display_name = $request->name;
        $role->description = $request->description;
        $role->syncPermissions($request->permission);

        return redirect()->route('role.index')->with('alert-success', 'Berhasil mengubah role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('role.index')->with('alert-success', 'Berhasil menghapus role');
    }
}
